package Net::OAuth::LP::Models::CVE;

use common::sense;
use Moo;
use Types::Standard qw(Str Int ArrayRef HashRef);
use Method::Signatures;
use List::Objects::WithUtils;

with('Net::OAuth::LP::Models');

has 'cves' => (is => 'rw',);

method entries {
  array(@{$self->cves->entries});
}

1;

__END__

=head1 NAME

Net::OAuth::LP::Models::CVE - Bug CVE Model

=head1 DESCRIPTION

Interface to setting/retrieving bug CVE information

=head1 SYNOPSIS

    my $c = Net::OAuth::LP::Client->new(consumer_key => 'blah',
                                        access_token => 'fdsafsda',
                                        access_token_secret => 'fdsafsda');

    my $b = Net::OAuth::LP::Models::Bug->new(c => $c);
    $b->find(1);
    say $b->cves->all;

=head1 ATTRIBUTES

=head2 B<cves>

=head1 METHODS

In addition to those listed this object inherits methods from List::Objects::WithUtils.

=head2 B<new>

    my $cve = Net::OAuth::LP::Models::CVE->new(cves => $bug->CVE);

=cut
